<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class m_prestasi extends CI_Model {

	public function get() {
		$this->db->order_by('tanggal_prestasi', 'ASC');
		return $this->db->get('tb_m_prestasi')->result();
	}

	public function insert($data, $table) {
		$this->db->insert($table, $data);
	}

	public function edit($id, $data, $table) {
		$this->db->where($id);
		$this->db->update($table, $data);
	}

	public function delete($id, $table) {
		$this->db->where_in('id', $id);
		$this->db->delete($table);
	}

	public function getPrestasiLimit() {
		$this->db->order_by('tanggal_prestasi', 'DESC');
		return $this->db->get('tb_m_prestasi', 3)->result();
	}

}    
