<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class m_bkk extends CI_Model {

	public function getLowongan() {
		return $this->db->query('SELECT * FROM tb_m_bkk WHERE kategori = "lowongan"')->result();
    }
    
	public function getSerapan() {
		return $this->db->query('SELECT * FROM tb_m_bkk WHERE kategori = "serapan"')->result();
	}

}   