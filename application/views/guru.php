<div class="team">
	<div class="courses">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="section_title_container text-center">
						<h2 class="section_title">Profil Guru</h2>
						<div class="section_subtitle"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vel gravida arcu. Vestibulum feugiat, sapien ultrices fermentum congue, quam velit venenatis sem</p></div>
					</div>
				</div>
			</div>
			<div class="row team_row">
				
				<!-- Team Item -->
				<?php foreach ($guru as $data) : ?>
					<div class="col-lg-3 col-md-6 team_col">
						<div class="team_item">
							<div class="team_image"><img src="<?= base_url('assets/images/guru_images/'.$data->foto); ?>" alt=""></div>
							<div class="team_body">
								<div class="team_title"><a href="#"><?= $data->nama_guru; ?></a></div>
								<div class="team_subtitle"><?= $data->mapel; ?></div>
								<div class="social_list">
									<ul>
										<li><a href="<?= $data->facebook; ?>"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
										<li><a href="<?= $data->instagram; ?>"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
										<li><a href="mail.com:<?= $data->gmail; ?>"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				<?php endforeach; ?>

			</div>
		</div>
	</div>
</div>