<div class="team">
<!-- Popular Courses -->

<div class="courses">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="section_title_container text-center">
						<h2 class="section_title">Struktur Organisasi</h2>
						<div class="section_subtitle"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vel gravida arcu. Vestibulum feugiat, sapien ultrices fermentum congue, quam velit venenatis sem</p></div>
					</div>
				</div>
			</div>
			<div class="row courses_row">
				
				<!-- Course -->
				<?php foreach($struktur_organisasi as $data) : ?>
				<div class="col-lg-12 course_col mt-4">
					<div class="course">
						<a href="<?= base_url('assets/images/struktur_images/'.$data->foto); ?>" class="single-popup-photo">
                                <div class="course_image"><img style="width:100%;cursor:pointer;" src="<?= base_url('assets/images/struktur_images/'.$data->foto); ?>" ></div>
                        </a>
						<div class="course_body">
							<h3 class="course_title"><a href="course.html">Struktur Organisasi</a></h3>
							<!-- <div class="course_teacher">SMKN 1</div> -->
							<div class="course_text">
								<p><?= $data->deskripsi; ?></p>
							</div>
						</div>
						<div class="course_footer">
						</div>
					</div>
				</div>
				<?php endforeach; ?>


			</div>
		</div>
	</div>
</div>    