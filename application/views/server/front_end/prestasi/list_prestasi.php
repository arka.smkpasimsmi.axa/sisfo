<div class="page-header">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <i class="ik ik-user" style="background-color: #6692e9;"></i>
                <div class="d-inline">
                    <h5>List Prestasi</h5>
                    <span>Kontrol Jurusan Sekolah disini</span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <nav class="breadcrumb-container" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?= base_url('Admin') ?>"><i class="ik ik-home"></i></a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="<?= base_url('Admin'); ?>">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Prestasi</li>
                </ol>
            </nav>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
              <h3 class="col-10">Guru</h3>
              <div class="col-2">
                <button type="button" class="btn ml-auto text-light tema-biru" data-toggle="modal" data-target="#exampleModal" >
                  <i class="ik ik-user-plus"></i>Tambah
                </button>
              </div>      
            </div>
            <div class="card-body mx-auto">
                <div id="data_table_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                    <div class="row mx-2">
                        <div class="col-sm-12">
                            <table id="data_table" class="table dataTable no-footer" role="grid" aria-describedby="data_table_info">
                    <thead>
                        <tr role="row">
                            <th class="sorting_asc text-center" tabindex="0" aria-controls="data_table" rowspan="1" colspan="1" style="width: 62.9px;" aria-sort="ascending" aria-label="Id: activate to sort column descending">Id</th>
                            <th class="nosort sorting_disabled text-center" rowspan="1" colspan="1" style="width: 104.467px;" aria-label="Avatar">Foto</th>
                            <th class="sorting" tabindex="0" aria-controls="data_table" rowspan="1" colspan="1" style="width: 209.45px;" aria-label="Name: activate to sort column ascending">Nama Kejuaraan</th>
                            <th class="sorting" tabindex="0" aria-controls="data_table" rowspan="1" colspan="1" style="width: 322.867px;" aria-label="Email: activate to sort column ascending">Deskripsi</th>
                            <th class="sorting" tabindex="0" aria-controls="data_table" rowspan="1" colspan="1" style="width: 322.867px;" aria-label="Email: activate to sort column ascending">Tanggal</th>
                            <th class="sorting" tabindex="0" aria-controls="data_table" rowspan="1" colspan="1" style="width: 322.867px;" aria-label="Email: activate to sort column ascending"></th></tr>
                        </tr>

                    </thead>
                <tbody>     
                    <?php foreach ($prestasi as $d): ?>
                        <tr role="row" class="odd">
                                <td class="sorting_1 text-center"><?= $d->id?></td>
                                <td class="text-center">
                                    <a href="<?= base_url('assets/images/prestasi_images/'.$d->foto) ?>" data-lightbox="mygallery" data-title="<?= $d->nama_prestasi ?>">
                                        <img src="<?= base_url('assets/images/prestasi_images/'.$d->foto); ?>" width="70" height="70">
                                    </a>
                                </td>
                                <td><a href="<?= base_url('Admin_prestasi/detailPrestasi/'.$d->id) ?>" style="color:#6692e9;"><?= $d->nama_prestasi ?></a></td>
                                <td><?= $d->deskripsi ?></td>
                                <td><?= $d->tanggal_prestasi ?></td>
                                <td>&nbsp;</td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                </table>
            </div>
        </div>
    </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card-body">
            <form method="POST" action="<?= base_url('Admin_prestasi/insertPrestasi') ?>" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="exampleInputUsername1">Nama Kejuaraan</label>
                    <input type="text" class="form-control" id="exampleInputUsername1" placeholder="Nama Kejuaraan" name="nama_prestasi">
                    <small class="text-danger"><i><?= form_error('nama_prestasi') ?></i></small>
                </div>
                <div class="form-group">
                    <label>Foto</label>
                    <div class="input-group col-xs-12">
                        <input type="file" class="form-control file-upload-info" placeholder="Upload Image" name="foto">
                        <span class="input-group-append">
                        <button class="file-upload-browse btn btn-primary" style="pointer-events: none;">Upload</button>
                        </span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleTextarea1">Tanggal</label>
                    <input id="dropper-default" class="form-control" type="text" placeholder="Tanggal Prestasi" readonly="readonly" name="tgl_prestasi">
                    <small class="text-danger"><i><?= form_error('tgl_prestasi') ?></i></small>
                </div>
                <div class="form-group">
                    <label for="exampleTextarea1">Deskripsi</label>
                    <textarea class="form-control" id="exampleTextarea1" rows="4" name="deskripsi"></textarea>
                    <small class="text-danger"><i><?= form_error('deskripsi') ?></i></small>
                </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn tema-biru text-light">Save changes</button>
        </form>
      </div>
    </div>
  </div>
</div>