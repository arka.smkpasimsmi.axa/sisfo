<?php foreach($info_sekolah as $data) : ?>
<div class="page-header">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <i class="ik ik-file-text" style="background-color:#6692e9;"></i>
                <div class="d-inline">
                    <h5>Info Sekolah</h5>
                    <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <nav class="breadcrumb-container" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="../index.html"><i class="ik ik-home"></i></a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="<?= base_url('Admin'); ?>">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Informasi Sekolah</li>
                </ol>
            </nav>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-4 col-md-5">
        <div class="card">
            <div class="card-body">
                <div class="text-center">
                    <h4 class="card-title mt-10"><?= $data->nama_sekolah; ?></h4>
                    <p class="card-subtitle mt-10"><?= $data->kota; ?></p>
                </div>
            </div>
            <hr class="mb-0"> 
            <div class="card-body"> 
                <small class="text-muted d-block">Alamat </small>
                <h6><?= $data->alamat; ?></h6> 
                <small class="text-muted d-block pt-10">No Telepon</small>
                <h6><?= $data->telepon; ?></h6> 
                <small class="text-muted d-block pt-10">Email</small>
                <h6><?= $data->email_sekolah; ?></h6> 
            </div>
        </div>
    </div>
    <div class="col-lg-8 col-md-7">
        <div class="card">
            <ul class="nav nav-pills custom-pills" id="pills-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active show" id="pills-setting-tab" data-toggle="pill" href="#previous-month" role="tab" aria-controls="pills-setting" aria-selected="false">Edit</a>
                </li>
            </ul>
            <div class="tab-content" id="pills-tabContent">
                <div class="tab-pane fade active show" id="previous-month" role="tabpanel" aria-labelledby="pills-setting-tab">
                    <div class="card-body">
                        <form class="form-horizontal" method="POST" action="<?= base_url('Info_sekolah/postEdit/'.$data->id) ?>" enctype="multipart/form-data" id="form-edit">
                            <div class="form-group">
                                <label for="example-name">Nama Sekolah</label>
                                <input type="text" placeholder="Nama Sekolah..." class="form-control" name="nama_sekolah" value="<?= $data->nama_sekolah; ?>">
                            </div>
                            <div class="form-group">
                                <label for="example-email">Kota</label>
                                <input type="text" placeholder="Kota..." class="form-control" name="kota" value="<?= $data->kota ?>">
                            </div>
                            <div class="form-group">
                                <label for="example-phone">Alamat</label>
                                <input type="text" placeholder="Alamat..." name="alamat" class="form-control" value="<?= $data->alamat ?>">
                            </div>
                            <div class="form-group">
                                <label for="example-phone">No.Telepon</label>
                                <input type="text" placeholder="No Telepon..." name="telepon" class="form-control" value="<?= $data->telepon; ?>">
                            </div>
                            <div class="form-group">
                                <label for="example-phone">Email Sekolah</label>
                                <input type="email" placeholder="Alamat email..." name="email_sekolah" class="form-control" value="<?= $data->email_sekolah; ?>">
                            </div>
                            <button class="btn btn-success" type="submit" id="btn-edit-submit">Update Informasi</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endforeach; ?>