<div class="team">
	<div class="contact">

		<!-- Contact Info -->
		<div class="contact_info_container">
			<div class="container">
            <div class="mt-5">
            <div class="col">
					<div class="section_title_container text-center pt-5">
						<h2 class="section_title">Detail Lowongan Pekerjaan</h2>
					</div>
				</div>
            </div>
				<div class="row pt-5">
					<!-- Detail Lowongan -->
					<div class="col-lg-8">
                            <?php foreach($lowongan as $data) : ?>
                                <div class="col-lg-12 course_col mt-4">
                                    <a href="<?= base_url('assets/images/bkk_images/'.$data->item); ?>" class="single-popup-photo">
                                            <div class="course_image"><img style="width:100%;max-height: 340px;object-fit: cover;object-position: center;cursor:pointer;" src="<?= base_url('assets/images/bkk_images/'.$data->item); ?>" ></div>
                                    </a>
                                    <div class="mt-5 pt-1">
                                        <h4 class="course_title"><?= $data->judul ?></h4>
                                        <!-- <div class="course_teacher">SMKN 1</div> -->
                                        <div class="course_text">
                                            <p><?= $data->deskripsi; ?></p>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
					</div>

					<!-- Contact Info -->
					<div class="col-lg-4">
                            <?php foreach($lowongan as $data) : ?>
                                <div class="col-lg-12 course_col mt-4">
                                    <div class="mt-4">
                                        <h3 class="course_title">Informasi Perusahaan</h3>
						            <div class="contact_info">  
                                        <div class="contact_info_location mt-4">
                                            <div class="contact_info_location_title"><h4><?= $data->judul ?></h4></div>
                                                <ul class="location_list mt-3">
                                                    <small>Kota</small>
                                                    <li><?= $data->kota ?></li>
                                                    <small>Alamat</small>
                                                    <li><?= $data->alamat ?></li>
                                                    <small>No.Telepon</small>
                                                    <li><?= $data->no_telp ?></li>
                                                    <small>Email</small>
                                                    <li><?= $data->email_perusahaan ?></li>
                                                </ul>
                                            </div>
                                    </div>        
                                    </div>
                                </div>
                            <?php endforeach; ?>
					</div>
					<!-- <div class="col-lg-6">
						<div class="contact_info">
							<div class="contact_info_title">Informasi Sekolah</div>
							<div class="contact_info_text">
								<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a distribution of letters.</p>
							</div>
							<div class="contact_info_location">
								<div class="contact_info_location_title">NM SKLH</div>
								<ul class="location_list">
									<small>Kota</small>
									<li>SKBM</li>
									<small>Alamat</small>
									<li>NYOMPLoNG</li>
									<small>No.Telepon</small>
									<li>TELP</li>
									<small>Email</small>
									<li>EMAIl</li>
								</ul>
							</div>
						</div>
					</div> -->
					
				</div>
			</div>
		</div>
	</div>
</div>