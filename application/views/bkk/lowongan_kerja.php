<div class="team">
<!-- Popular Courses -->

	<div class="courses">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="section_title_container text-center">
						<h2 class="section_title">Lowongan Kerja</h2>
						<div class="section_subtitle"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vel gravida arcu. Vestibulum feugiat, sapien ultrices fermentum congue, quam velit venenatis sem</p></div>
					</div>
				</div>
			</div>
			<div class="row courses_row">
				
				<?php foreach($lowongan_kerja as $data) : ?>
				<!-- Course -->
				<div class="col-lg-4 course_col mt-4">
					<div class="course">
						<a href="<?= base_url('assets/images/bkk_images/'.$data->item); ?>" class="single-popup-photo">
                            <div class="course_image"><img style="width:100%;max-height: 252px;object-fit: cover;object-position: center;cursor:pointer;" src="<?= base_url('assets/images/bkk_images/'.$data->item); ?>" ></div>
                        </a>
						<div class="course_body">
							<h3 class="course_title"><a href="<?= base_url('Bkk/detailLowongan/'.$data->id) ?>"><?= $data->judul; ?></a></h3>
							<div class="course_teacher"></div>
							<div class="course_text">
								<p style="width:100%;height:121px;overflow:hidden;text-overflow:ellipsis;"><?= $data->deskripsi; ?></p>
							</div>
						</div>
						<div class="course_footer">
						</div>
					</div>
				</div>
				<?php endforeach; ?>

			</div>
		</div>
	</div>
</div>


