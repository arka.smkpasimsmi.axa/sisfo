<!DOCTYPE html>
<html lang="en">
	<head>
		<?php $beranda = $this->crud->get('tb_m_beranda') ?>
		<?php $kontak = $this->crud->get('tb_m_kontak') ?>

		<?php foreach ($beranda as $data) : ?>
			<title><?= $data->nama_sekolah.' - '.$title ?></title>
        	<link rel="icon" href="<?= base_url('assets/images/beranda_images/'.$data->ikon) ?>" type="image/x-icon" />
		<?php endforeach; ?>

		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="description" content="Unicat project">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<link rel="stylesheet" type="text/css" href="<?= base_url('assets/styles/bootstrap4/bootstrap.min.css') ?>">
		<link rel="stylesheet" type="text/css" href="<?= base_url('assets/plugins/font-awesome-4.7.0/css/font-awesome.min.css') ?>">
		<link rel="stylesheet" type="text/css" href="<?= base_url('assets/plugins/OwlCarousel2-2.2.1/owl.carousel.css') ?>">
		<link rel="stylesheet" type="text/css" href="<?= base_url('assets/plugins/OwlCarousel2-2.2.1/owl.theme.default.css') ?>">
		<link rel="stylesheet" type="text/css" href="<?= base_url('assets/plugins/OwlCarousel2-2.2.1/animate.css') ?>">
		<link rel="stylesheet" type="text/css" href="<?= base_url('assets/styles/main_styles.css') ?>">
		<link rel="stylesheet" type="text/css" href="<?= base_url('assets/styles/responsive.css') ?>">
		<link rel="stylesheet" type="text/css" href="<?= base_url('assets/server_assets/plugins/magnific-popup/dist/magnific-popup.css') ?>">
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/server_assets/plugins/jquery-toast-plugin/dist/jquery.toast.min.css') ?>">

		<?php if($this->uri->segment(2) == "jurusanSekolah") : ?>
			<link rel="stylesheet" type="text/css" href="<?= base_url('assets/styles/about.css') ?>">
			<link rel="stylesheet" type="text/css" href="<?= base_url('assets/styles/about_responsive.css') ?>">
		<?php endif; ?>

		<?php if($this->uri->segment(2) == "contactUs" || $this->uri->segment(2) == "ppdb" || $this->uri->segment(2) == "insertPpdb" || $this->uri->segment(2) == "login") : ?>
			<link rel="stylesheet" type="text/css" href="<?= base_url('assets/styles/contact.css') ?>">
			<link rel="stylesheet" type="text/css" href="<?= base_url('assets/styles/contact_responsive.css') ?>">
		<?php endif; ?>

		<?php if($this->uri->segment(2) == "Bkk" || $this->uri->segment(2) == "lowongan_kerja" || $this->uri->segment(2) == "serapan_kerja") : ?>
			<link rel="stylesheet" type="text/css" href="<?= base_url('assets/styles/about.css') ?>">
			<link rel="stylesheet" type="text/css" href="<?= base_url('assets/styles/about_responsive.css') ?>">
		<?php endif; ?>


	</head>
	<body>

		<div class="super_container">

			<!-- Header -->
			<?php foreach($kontak as $data) : ?>
			<header class="header">
				<!-- Top Bar -->
				<div class="top_bar">
					<div class="top_bar_container">
						<div class="container">
							<div class="row">
								<div class="col">
									<div class="top_bar_content d-flex flex-row align-items-center justify-content-start">
										<ul class="top_bar_contact_list">
											<li><div class="question">Ada pertanyaan?</div></li>
											<li>
												<i class="fa fa-phone" aria-hidden="true"></i>
												<div><?= $data->telepon ?></div>
											</li>
											<li>
												<i class="fa fa-envelope-o" aria-hidden="true"></i>
												<div><?= $data->email_sekolah ?></div>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>				
				</div>
			<?php endforeach; ?>
			<?php foreach($beranda as $data) : ?>
				<!-- Header Content -->
				<div class="header_container">
					<div class="container">
						<div class="row">
							<div class="col">
								<div class="header_content d-flex flex-row align-items-center justify-content-start">
									<div class="logo_container">
										<a href="#">
										<div class="row align-items-lg-center align-items-sm-center">
											<div class="logo_text">
												<img style="width:50px;" src="<?= base_url('assets/images/beranda_images/'.$data->ikon); ?>" alt="">
											</div>
												<h3 class="ml-3" style="width:120px;word-wrap:break-word;"><?= $data->teks_logo ?></h3>	
										</div>

										</a>
									</div>
			<?php endforeach; ?>						
									<nav class="main_nav_contaner ml-auto">
										<ul class="main_nav">
											<li class="<?php if($this->uri->segment(1) == "") {echo 'active';} ?>"><a href="<?= base_url() ?>">Beranda</a></li>
											<li class="<?php if($this->uri->segment(2) == "profil") {echo 'active';} ?> dropdown">
												<a class="dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Profil</a>
												<div class="dropdown-menu" aria-labelledby="navbarDropdown">
										          <a class="dropdown-item <?php if($this->uri->segment(2) == "strukturOrganisasi") {echo 'active';} ?>" href="<?= base_url('Page/strukturOrganisasi') ?>">Struktur Organisasi</a>
										          <a class="dropdown-item <?php if($this->uri->segment(2) == "jurusanSekolah") {echo 'active';} ?>" href="<?= base_url('Page/jurusanSekolah') ?>">Jurusan Sekolah</a>
										        </div>
											</li>
											<li class="<?php if($this->uri->segment(2) == "galeri") {echo 'active';} ?> dropdown"><a class="dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Galeri</a>
												<div class="dropdown-menu" aria-labelledby="navbarDropdown">
										          <a class="dropdown-item <?php if($this->uri->segment(2) == "foto") {echo 'active';} ?>" href="<?= base_url('Page/foto') ?>">Foto</a>
										          <a class="dropdown-item <?php if($this->uri->segment(2) == "video") {echo 'active';} ?>" href="<?= base_url('Page/video') ?>">Video</a>
										        </div>
											</li>
											<li class="<?php if($this->uri->segment(2) == "guru") {echo 'active';} ?>"><a href="<?= base_url('Page/guru') ?>">Guru</a></li>
											<li class="<?php if($this->uri->segment(2) == "prestasi") {echo 'active';} ?>"><a href="<?= base_url('Page/prestasi') ?>">Prestasi</a></li>
											<li class="<?php if($this->uri->segment(2) == "ppdb") {echo 'active';} ?>"><a href="<?= base_url('Page/ppdb') ?>">PPDB</a></li>
											<li class="<?php if($this->uri->segment(2) == "bkk") {echo 'active';} ?> dropdown"><a class="dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">BKK</a>
												<div class="dropdown-menu" aria-labelledby="navbarDropdown">
										          <a class="dropdown-item <?php if($this->uri->segment(2) == "lowongan_kerja") {echo 'active';} ?>" href="<?= base_url('Bkk/lowongan_kerja') ?>">Lowongan Kerja</a>
										          <a class="dropdown-item <?php if($this->uri->segment(2) == "serapan_kerja") {echo 'active';} ?>" href="<?= base_url('Bkk/serapan_kerja') ?>">Serapan Kerja</a>
										        </div>
											</li>
											<li class="<?php if($this->uri->segment(2) == "contactUs") {echo 'active';} ?>"><a href="<?= base_url('Page/contactUs') ?>">Hubungi Kami</a></li>
											<li class="<?php if($this->uri->segment(2) == "login") {echo 'active';} ?>"><a href="<?= base_url('Page/login') ?>">Login</a></li>
										</ul>

										<!-- Hamburger -->

										<div class="hamburger menu_mm">
											<i class="fa fa-bars menu_mm" aria-hidden="true"></i>
										</div>
									</nav>

								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- Header Search Panel -->
				<div class="header_search_container">
					<div class="container">
						<div class="row">
							<div class="col">
								<div class="header_search_content d-flex flex-row align-items-center justify-content-end">
									<form action="#" class="header_search_form">
										<input type="search" class="search_input" placeholder="Search" required="required">
										<button class="header_search_button d-flex flex-column align-items-center justify-content-center">
											<i class="fa fa-search" aria-hidden="true"></i>
										</button>
									</form>
								</div>
							</div>
						</div>
					</div>			
				</div>			
			</header>

			<!-- Menu -->

			<div class="menu d-flex flex-column align-items-end justify-content-start text-right menu_mm trans_400">
				<div class="menu_close_container"><div class="menu_close"><div></div><div></div></div></div>
				<div class="search">
					<form action="#" class="header_search_form menu_mm">
						<input type="search" class="search_input menu_mm" placeholder="Search" required="required">
						<button class="header_search_button d-flex flex-column align-items-center justify-content-center menu_mm">
							<i class="fa fa-search menu_mm" aria-hidden="true"></i>
						</button>
					</form>
				</div>
				<nav class="menu_nav">
					<ul class="menu_mm">
						<li class="<?php if($this->uri->segment(1) == "") {echo 'active';} ?>"><a href="<?= base_url() ?>">Beranda</a></li>
							<li class="<?php if($this->uri->segment(2) == "profil") {echo 'active';} ?> dropdown">
								<a class="dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Profil</a>
								<div class="dropdown-menu" aria-labelledby="navbarDropdown">
									<a class="dropdown-item <?php if($this->uri->segment(2) == "strukturOrganisasi") {echo 'active';} ?>" href="<?= base_url('Page/strukturOrganisasi') ?>">Struktur Organisasi</a>
									<a class="dropdown-item <?php if($this->uri->segment(2) == "jurusanSekolah") {echo 'active';} ?>" href="<?= base_url('Page/jurusanSekolah') ?>">Jurusan Sekolah</a>
								</div>
							</li>
							<li class="<?php if($this->uri->segment(2) == "galeri") {echo 'active';} ?> dropdown"><a class="dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Galeri</a>
								<div class="dropdown-menu" aria-labelledby="navbarDropdown">
									<a class="dropdown-item <?php if($this->uri->segment(2) == "foto") {echo 'active';} ?>" href="<?= base_url('Page/foto') ?>">Foto</a>
									<a class="dropdown-item <?php if($this->uri->segment(2) == "video") {echo 'active';} ?>" href="<?= base_url('Page/video') ?>">Video</a>
								</div>
							</li>
							<li class="<?php if($this->uri->segment(2) == "guru") {echo 'active';} ?>"><a href="<?= base_url('Page/guru') ?>">Guru</a></li>
							<li class="<?php if($this->uri->segment(2) == "prestasi") {echo 'active';} ?>"><a href="<?= base_url('Page/prestasi') ?>">Prestasi</a></li>
							<li class="<?php if($this->uri->segment(2) == "ppdb") {echo 'active';} ?>"><a href="<?= base_url('Page/ppdb') ?>">PPDB</a></li>
							<li class="<?php if($this->uri->segment(2) == "bkk") {echo 'active';} ?> dropdown"><a class="dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">BKK</a>
								<div class="dropdown-menu" aria-labelledby="navbarDropdown">
									<a class="dropdown-item <?php if($this->uri->segment(2) == "lowongan_kerja") {echo 'active';} ?>" href="<?= base_url('Bkk/lowongan_kerja') ?>">Lowongan Kerja</a>
									<a class="dropdown-item <?php if($this->uri->segment(2) == "serapan_kerja") {echo 'active';} ?>" href="<?= base_url('Bkk/serapan_kerja') ?>">Serapan Kerja</a>
								</div>
							</li>
							<li class="<?php if($this->uri->segment(2) == "contactUs") {echo 'active';} ?>"><a href="<?= base_url('Page/contactUs') ?>">Hubungi Kami</a></li>
						<li class="<?php if($this->uri->segment(2) == "login") {echo 'active';} ?>"><a href="<?= base_url('Page/login') ?>">Login</a></li>
					</ul>
				</nav>
			</div>
	