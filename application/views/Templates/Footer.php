		<!-- Footer -->
<?php $kontak = $this->crud->get('tb_m_kontak'); ?>
<?php foreach ($kontak as $data) : ?>
		<footer class="footer">
			<div class="footer_background" style="background-image:url(images/footer_background.png)"></div>
			<div class="container">
				<div class="row footer_row">
					<div class="col">
						<div class="footer_content">
							<div class="row">

								<div class="col-lg-6 footer_col">
						
									<!-- Footer About -->
									<div class="footer_section footer_about">
										<div class="footer_logo_container">
											<a href="#">
												<div class="footer_logo_text"><?= $data->nama_sekolah ?></div>
											</a>
										</div>
										<div class="footer_about_text">
											<p>Sosial media Sekolah kami.</p>
										</div>
										<div class="footer_social">
											<ul>
												<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
												<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
												<li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
												<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
											</ul>
										</div>
									</div>
									
								</div>

								<div class="col-lg-6 footer_col">
						
									<!-- Footer Contact -->
									<div class="footer_section footer_contact">
										<div class="footer_title">Contact Us</div>
										<div class="footer_contact_info">
											<ul>
													<li>Email: <?= $data->email_sekolah ?></li>
													<li>Telepon:  <?= $data->telepon ?></li>
													<li><?= $data->kota.', '.$data->alamat?></li>
											</ul>
										</div>
									</div>
									
								</div>

							

							</div>
						</div>
					</div>
				</div>

				<div class="row copyright_row">
					<div class="col">
						<div class="copyright d-flex flex-lg-row flex-column align-items-center justify-content-start">
							<div class="cr_text"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
	Copyright &copy; <script>document.write(new Date().getFullYear());</script> with <i class="fa fa-heart-o" aria-hidden="true"></i> by RPL Pasim
	<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></div>
						</div>
					</div>
				</div>
			</div>
<?php endforeach; ?>
		</footer>
	</div>

	<script src="<?= base_url('assets/js/jquery-3.2.1.min.js') ?>"></script>
	<script src="<?= base_url('assets/styles/bootstrap4/popper.js') ?>"></script>
	<script src="<?= base_url('assets/styles/bootstrap4/bootstrap.min.js') ?>"></script>
	<script src="<?= base_url('assets/plugins/greensock/TweenMax.min.js') ?>"></script>
	<script src="<?= base_url('assets/plugins/greensock/TimelineMax.min.js') ?>"></script>
	<script src="<?= base_url('assets/plugins/scrollmagic/ScrollMagic.min.js') ?>"></script>
	<script src="<?= base_url('assets/plugins/greensock/animation.gsap.min.js') ?>"></script>
	<script src="<?= base_url('assets/plugins/greensock/ScrollToPlugin.min.js') ?>') ?>"></script>
	<script src="<?= base_url('assets/plugins/OwlCarousel2-2.2.1/owl.carousel.js') ?>"></script>
	<script src="<?= base_url('assets/plugins/easing/easing.js') ?>"></script>
	<script src="<?= base_url('assets/plugins/parallax-js-master/parallax.min.js') ?>"></script>
	<script src="<?= base_url('assets/js/custom.js') ?>"></script>

	
	
	<script src="<?= base_url('assets/plugins/colorbox/jquery.colorbox-min.js') ?>"></script>
	<script src="<?= base_url('assets/js/about.js') ?>"></script>
	
	<script src="<?= base_url('assets/js/blog.js') ?>"></script>
	<script src="<?= base_url('assets/plugins/masonry/masonry.js') ?>"></script>
	<script src="<?= base_url('assets/plugins/video-js/video.min.js') ?>"></script>
	
	<script src="<?= base_url('assets/js/blog_single.js') ?>"></script>
	
	<script src="<?= base_url() ?>assets/server_assets/plugins/magnific-popup/dist/jquery.magnific-popup.min.js"></script>
    <script src="<?= base_url() ?>assets/server_assets/js/custom.js"></script>
	
	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCIwF204lFZg1y4kPSIhKaHEXMLYxxuMhA"></script>
	<script src="<?= base_url('assets/plugins/marker_with_label/marker_with_label.js') ?>"></script>
	<script src="<?= base_url('assets/js/contact.js') ?>"></script>

    <script src="<?= base_url('assets/server_assets/plugins/jquery-toast-plugin/dist/jquery.toast.min.js') ?>"></script>
    <script src="<?= base_url('assets/server_assets/js/alerts.js') ?>"></script>
	
    <script>
        $(document).ready(function(){
            <?php if ($this->session->flashdata('success')) : ?>
                showSuccessToast("<?= $this->session->flashdata('success'); ?>"); //function ada di assets/server_assets/js/alerts.js
            <?php endif; ?>

            <?php if ($this->session->flashdata('fail')) : ?>
                showDangerToast("<?= $this->session->flashdata('fail'); ?>");
            <?php endif; ?>

            <?php if ($this->session->flashdata('info')) : ?>
                showInfoToast("<?= $this->session->flashdata('info'); ?>");
            <?php endif; ?>

             <?php if ($this->session->flashdata('warning')) : ?>
                showWarningToast("<?= $this->session->flashdata('warning'); ?>");
            <?php endif; ?>

          });
    </script>

	</body>
</html>