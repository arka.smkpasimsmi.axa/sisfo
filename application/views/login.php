<div class="team">	
		<div class="contact_info_container">
			<div class="container">
				<div class="row">
					<div class="col-lg-6 offset-lg-3">
						<div class="contact_form">
							<div align="center" class="contact_info_title">Login</div>
							<form action="" class="comment_form" method="POST">
								<div>
									<div class="form_title">Email</div>
									<input type="email" class="comment_input" name="email" value="<?= set_value('email'); ?>" autofocus>
									<small style="font-style: italic;"><?= '<style>small > p{color:red;}</style>', form_error('email'); ?></small>
								</div>
								<div>
									<div class="form_title">Password</div>
									<input type="password" class="comment_input" name="password">
									<small style="font-style: italic;"><?= '<style>small > p{color:red;}</style>', form_error('password'); ?></small>
								</div>
								<div>
									<button type="submit" class="comment_button trans_200">Login</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
</div>