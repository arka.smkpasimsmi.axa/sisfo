<div class="team">
	<div class="contact">

		<!-- Contact Info -->
		<div class="contact_info_container">
			<div class="container">
				<div class="row">

					<!-- Contact Form -->
					<div class="col-lg-6">
						<div class="contact_form">
							<div class="contact_info_title">Tinggalkan Balasan</div>
							<form action="" class="comment_form" method="POST">
								<div>
									<div class="form_title">Nama</div>
									<input type="text" class="comment_input" name="nama" autofocus value="<?= set_value('nama'); ?>">
									<small style="font-style: italic;"><?= '<style>small > p{color:red;}</style>', form_error('nama'); ?></small>
								</div>
								<div>
									<div class="form_title">Email</div>
									<input type="text" class="comment_input" name="email" value="<?= set_value('email'); ?>">
									<small style="font-style: italic;"><?= '<style>small > p{color:red;}</style>', form_error('email'); ?></small>
								</div>
								<div>
									<div class="form_title">Pesan</div>
									<textarea class="comment_input comment_textarea" name="pesan"><?= set_value('pesan'); ?></textarea>	<small style="font-style: italic;"><?= '<style>small > p{color:red;}</style>', form_error('pesan'); ?></small>
								</div>
								<div>
									<button type="submit" class="comment_button trans_200">Kirim</button>
								</div>
							</form>
						</div>
					</div>

					<!-- Contact Info -->
					<?php foreach($contact_us as $data) : ?>
					<div class="col-lg-6">
						<div class="contact_info">
							<div class="contact_info_title">Informasi Sekolah</div>
							<div class="contact_info_text">
								<!-- <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a distribution of letters.</p> -->
							</div>
							<div class="contact_info_location">
								<div class="contact_info_location_title"><?= $data->nama_sekolah; ?></div>
								<ul class="location_list">
									<small>Kota</small>
									<li><?= $data->kota; ?></li>
									<small>Alamat</small>
									<li><?= $data->alamat; ?></li>
									<small>No.Telepon</small>
									<li><?= $data->telepon; ?></li>
									<small>Email</small>
									<li><?= $data->email_sekolah; ?></li>
								</ul>
							</div>
						</div>
					</div>
					<?php endforeach; ?>

				</div>
			</div>
		</div>
	</div>
</div>